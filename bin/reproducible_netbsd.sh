#!/usr/bin/env bash

# Copyright 2014-2022 Holger Levsen <holger@layer-acht.org>
# released under the GPLv2


PAGE=netbsd/netbsd.html
TIMEOUT="30m"
DEBUG=true

. /srv/jenkins/bin/common-functions.sh
common_init "${@}"

# common code for tests.reproducible-builds.org
. /srv/jenkins/bin/reproducible_common.sh

set -e
set -x

# Build for these architectures/ports.
declare -a machines	# Contains <machine>-<arch> (= <port>-<cpu>) pairs.
machines+=( sparc64-sparc64 )
#machines+=( amd64-x86_64 )

# How much Noise shall NetBSD's build process generate?
declare -a nb_noise
nb_noise=( -N 1 )

# Temporary debug function.
DG() {
	echo "LOGJJ: $(pwd) - ${*}"
}

cleanup_tmpdirs() {
	DG "In cleanup_tmpdirs, TMPDIR=${TMPDIR}, TMPBUILDDIR=${TMPBUILDDIR}"
	rm -r "${TMPDIR}"
	rm -r "${TMPBUILDDIR}"
}

create_results_dirs() {
	DG "In create_results_dirs"
	mkdir -p "${BASE}/netbsd/dbd"
}

# $1 - build ("b1", "b2", ...)
# $2 - machine ("x86_64-amd64")
# $3 - release dir
save_netbsd_results() {
	local run="${1}"; shift
	local full_machine="${1}"; shift
	local release_dir="${1}"; shift

	DG "save_netbsd_results: For build ${run} of machine ${full_machine}, copying ${release_dir} to $TMPDIR/${run}/${full_machine}"

	mkdir -p "${TMPDIR}/${run}/${full_machine}"
	(cd "${release_dir}" && tar cf - .) | ( cd "${TMPDIR}/${run}/${full_machine}" && tar xf -)
	find "${TMPDIR}/${run}/${full_machine}" \( -name MD5 -o -name SHA512 \) -exec rm {} \;
}

#
# main
#
TMPBUILDDIR=$(mktemp --tmpdir=/srv/workspace/chroots/   -d -t rbuild-netbsd-build-XXXXXXXX)   # used to build on tmpfs
TMPDIR=$(     mktemp --tmpdir=/srv/reproducible-results -d -t rbuild-netbsd-results-XXXXXXXX) # accessible in schroots, used to compare results
script_start_time=$(date -u +'%Y-%m-%d')
START=$(date +'%s')
trap cleanup_tmpdirs INT TERM EXIT

pushd "${TMPBUILDDIR}"
	DG "main: Checking our NetBSD src GIT tree"

	# Prepare sources.
	echo "============================================================================="
	echo "$(date -u) - Cloning the NetBSD git repository (which is synced with the NetBSD CVS repository)"
	echo "============================================================================="
	git clone --depth 1 https://github.com/NetBSD/src.git netbsd

	# Get current top commit infos.
	pushd netbsd
		DG "main: Fetching some GIT data items"
		netbsd_top_commit_log="$(git log -1)"
		netbsd_top_commit_rev=$(git describe --always)
		NETBSD_TIMESTAMP=$(git log -1 --format=%ct)
		echo "This is NetBSD ${netbsd_top_commit_rev}."
		echo
		git log -1
	popd

	# First round of builds. Run in a subshell to protect environment.
	(
		echo "============================================================================="
		echo "$(date -u) - Building NetBSD ${netbsd_top_commit_rev} - first build run."
		echo "============================================================================="
		export TZ="/usr/share/zoneinfo/Etc/GMT+12"
		for this_machine in "${machines[@]}" ; do
			DG "main: Building NB #1 for ${this_machine}"

			this_subdir="b1-${this_machine}"
			this_mach="$(echo "${this_machine}" | cut -f 1 -d -)"
			this_arch="$(echo "${this_machine}" | cut -f 2 -d -)"

			# Create a pristine linked tree.
			cp -Rl netbsd "${this_subdir}"

			# Build and store results.
			pushd "${this_subdir}"
				release_dir=_release_
				full_release_dir="$(realpath "${release_dir}")"
				mkdir -p "${release_dir}"

				DG "main: Actually building NB #1 for ${this_machine}, results in ${release_dir}"
				ionice -c 3 ./build.sh -j "${NUM_CPU}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" tools		|| true
				ionice -c 3 ./build.sh -j "${NUM_CPU}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" release		|| true
				ionice -c 3 ./build.sh -j "${NUM_CPU}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" iso-image		|| true
				ionice -c 3 ./build.sh -j "${NUM_CPU}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" install-image	|| true
				ionice -c 3 ./build.sh -j "${NUM_CPU}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" live-image	|| true

				save_netbsd_results b1 "${this_machine}" "${full_release_dir}"
				echo "${this_machine} done, first time."
			popd

			# Clean up.
			DG "main: Cleaning up NB #1 for ${this_machine}"
			rm -rf "${this_subdir}"
		done
	)

	# Second round of builds. Also in a subshell to not taint our own environment.
	(
		echo "============================================================================="
		echo "$(date -u) - Building NetBSD ${netbsd_top_commit_rev} - second build run."
		echo "============================================================================="
		export TZ="/usr/share/zoneinfo/Etc/GMT-14"
		export LANG="fr_CH.UTF-8"
		export LC_ALL="fr_CH.UTF-8"
		export PATH="/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/i/capture/the/path"
		export CAPTURE_ENVIRONMENT="I capture the environment"
		umask 0002
		new_num_cpu=$(( ${NUM_CPU} - 1 ))
		for this_machine in "${machines[@]}"; do
			DG "main: Building NB #2 for ${this_machine}"

			this_subdir="b2-${this_machine}"
			this_mach="$(echo "${this_machine}" | cut -f 1 -d -)"
			this_arch="$(echo "${this_machine}" | cut -f 2 -d -)"

			# Create a pristine linked tree.
			cp -Rl netbsd "${this_subdir}"

			# Build and store results.
			pushd "${this_subdir}"
				release_dir=_release_
				full_release_dir="$(realpath "${release_dir}")"
				mkdir -p "${release_dir}"

				DG "main: Actually building NB #1 for ${this_machine}, results in ${release_dir}"
				ionice -c 3 linux64 --uname-2.6 ./build.sh -j "${new_num_cpu}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" tools		|| true
				ionice -c 3 linux64 --uname-2.6 ./build.sh -j "${new_num_cpu}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" release		|| true
				ionice -c 3 linux64 --uname-2.6 ./build.sh -j "${new_num_cpu}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" iso-image		|| true
				ionice -c 3 linux64 --uname-2.6 ./build.sh -j "${new_num_cpu}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" install-image	|| true
				ionice -c 3 linux64 --uname-2.6 ./build.sh -j "${new_num_cpu}" -R "${full_release_dir}" -P -U -u -a "${this_arch}" -m "${this_mach}" "${nb_noise[@]}" live-image	|| true

				save_netbsd_results b2 "${this_machine}" "${full_release_dir}"
				echo "${this_machine} done, second time."
			popd

			# Clean up.
			DG "main: Cleaning up NB #2 for ${this_machine}"
			rm -rf "${this_subdir}"
		done
	)

	# Delete sources.
	DG "main: Cleaning initial NetBSD GIT checkout"
	rm -rf netbsd
popd	# from ${TMPBUILDDIR}


#
# Compare results and create a nice HTML page.
#

# Prepare a file list
declare -a file_list
declare -A file_list_reproducible
declare -A file_list_nonreproducible
tree "${TMPDIR}/b1"
tree "${TMPDIR}/b2"
file_list=( $( (cd "${TMPDIR}/b1" && find . -type f; cd "${TMPDIR}/b2" && find . -type f) | sort | uniq ) )


# XXX This is old code. Before cleaning it up, let's try to get the initial part running.

# Run diffoscope on the results.
diffoscope_version="$(schroot --directory /tmp -c "chroot:jenkins-reproducible-${DBDSUITE}-diffoscope" diffoscope -- --version 2>&1)"
echo "============================================================================="
echo "$(date -u) - Running ${diffoscope_version} on NetBSD build results..."
echo "============================================================================="
FILES_HTML=$(       mktemp --tmpdir="${TMPDIR}")
GOOD_FILES_HTML=$(  mktemp --tmpdir="${TMPDIR}")
BAD_FILES_HTML=$(   mktemp --tmpdir="${TMPDIR}")
GOOD_SECTION_HTML=$(mktemp --tmpdir="${TMPDIR}")
BAD_SECTION_HTML=$( mktemp --tmpdir="${TMPDIR}")
GOOD_FILES=0
ALL_FILES=0
create_results_dirs
cd "${TMPDIR}/b1"
tree .
for i in *; do
	pushd "${i}"
		for j in $(find * -type f |sort -u ) ; do
			let ALL_FILES+=1
			call_diffoscope "${i}" "${j}"
			if [ -f "${TMPDIR}/${i}/${j}.html" ]; then
				mkdir -p "${BASE}/netbsd/dbd/${i}/$(dirname "${j}")"
				mv "${TMPDIR}/${i}/${j}.html" "${BASE}/netbsd/dbd/${i}/${j}.html"
				echo "         <tr><td><a href=\"dbd/$i/$j.html\"><img src=\"/userContent/static/weather-showers-scattered.png\" alt=\"unreproducible icon\" /> $j</a> ($(get_filesize "${j}")) is unreproducible.</td></tr>" >> "${BAD_FILES_HTML}"
			else
				SHASUM=$(sha256sum "${j}" | cut -d " " -f1)
				echo "         <tr><td><img src=\"/userContent/static/weather-clear.png\" alt=\"reproducible icon\" /> $j ($SHASUM, $(get_filesize "${j}")) is reproducible.</td></tr>" >> "${GOOD_FILES_HTML}"
				let GOOD_FILES+=1
				rm -f "${BASE}/netbsd/dbd/${i}/${j}.html" # cleanup from previous (unreproducible) tests - if needed
			fi
		done
	popd

	if [ -s "${GOOD_FILES_HTML}" ]; then
		{
			echo "       <table><tr><th>Reproducible artifacts for <code>$i</code></th></tr>"
			cat "${GOOD_FILES_HTML}"
			echo "       </table>"
		} >> "${GOOD_SECTION_HTML}"
	fi
	rm "${GOOD_FILES_HTML}"

	if [ -s "${BAD_FILES_HTML}" ]; then
		{
			echo "       <table><tr><th>Unreproducible artifacts for <code>$i</code></th></tr>"
			cat "${BAD_FILES_HTML}"
			echo "       </table>"
		} >> "${BAD_SECTION_HTML}"
	fi
	rm "${BAD_FILES_HTML}"
done
GOOD_PERCENT=$(echo "scale=1 ; ($GOOD_FILES*100/$ALL_FILES)" | bc)
# Are we there yet?
if [ "$GOOD_PERCENT" = "100.0" ] ; then
	MAGIC_SIGN="!"
else
	MAGIC_SIGN="?"
fi

#
# Finally create the web page.
#
pushd "${TMPDIR}"
	mkdir netbsd
	cat > "${PAGE}" <<- EOF
<!DOCTYPE html>
<html lang="en-US">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Reproducible NetBSD $MAGIC_SIGN</title>
    <link rel='stylesheet' href='global.css' type='text/css' media='all' />
  </head>
  <body>
    <div id="logo">
      <img src="NetBSD-smaller.png" />
      <h1>Reproducible NetBSD $MAGIC_SIGN</h1>
    </div>
    <div class="content">
      <div class="page-content">
EOF
	write_page_intro NetBSD
	write_page "       <p>$GOOD_FILES ($GOOD_PERCENT%) out of $ALL_FILES built NetBSD files were reproducible in our test setup"
	if [ "$GOOD_PERCENT" = "100.0" ] ; then
		write_page "!"
	else
		write_page "."
	fi
	write_page "        These tests were last run on ${script_start_time} for rev ${netbsd_top_commit_rev} with -P (as of @${NETBSD_TIMESTAMP}) and were compared using ${diffoscope_version}.</p>"
	write_variation_table NetBSD
	cat "${BAD_SECTION_HTML}" >> "${PAGE}"
	cat "${GOOD_SECTION_HTML}" >> "${PAGE}"
	write_page "     <p><pre>"
	echo -n "${netbsd_top_commit_log}" >> "${PAGE}"
	write_page "     </pre></p>"
	write_page "    </div></div>"
	write_page_footer NetBSD
	publish_page
	rm -f "${FILES_HTML}" "${GOOD_FILES_HTML}" "${BAD_FILES_HTML}" "${GOOD_SECTION_HTML}" "${BAD_SECTION_HTML}"
popd

# The end.
calculate_build_duration
print_out_duration
irc_message reproducible-changes "$REPRODUCIBLE_URL/netbsd/ has been updated. ($GOOD_PERCENT% reproducible)"
echo "============================================================================="

# Remove everything, we don't need it anymore...
cleanup_tmpdirs
trap - INT TERM EXIT
