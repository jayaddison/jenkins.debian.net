#!/bin/bash
#
# Copyright 2024-2025 Holger Levsen (holger@layer-acht.org)
# released under the GPLv2

set -e

#
# create graph for $ARCH.reproduce.debian.net
#
if [ -z "$1" ] ; then
	echo "need an architecture."
	exit 1
fi

ARCH=$1
DB=/srv/rebuilderd/$ARCH/rebuilderd.db
CSV=/srv/rebuilderd/$ARCH/stats/rebuilderd.csv
PNG=/srv/rebuilderd/$ARCH/stats/rb.png
TODAY=$(date '+%Y-%m-%d' -u)
DUMMY_FILE=$(mktemp -t rebuilderd-graph-XXXXXXXX)
touch -d "$(date '+%Y-%m-%d') 00:00 UTC" $DUMMY_FILE

#
# init .csv file if needed
#
if [ ! -f $CSV ] ; then
	echo "; date, good, unknown, bad" > $CSV
fi

#
# update .csv if needed
#
if ! grep -q $TODAY $CSV ; then
	GOOD=$(echo "SELECT count(*) FROM packages WHERE status='GOOD'" | sqlite3 $DB)
	BAD=$(echo "SELECT count(*) FROM packages WHERE status='BAD'" | sqlite3 $DB)
	UNKNOWN=$(echo "SELECT count(*) FROM packages WHERE status!='BAD' AND status!='GOOD'" | sqlite3 $DB)
	echo "$TODAY, $GOOD, $UNKNOWN, $BAD" >> $CSV
fi

#
# update png if needed
#
if [ ! -f $PNG ] || [ $DUMMY_FILE -nt $PNG ] ; then
	XLABEL="reproduced arch:$ARCH packages in trixie"
	GOOD=$(tail -1 $CSV | tr -d , | cut -d ' ' -f2)
	BAD=$(tail -1 $CSV | tr -d , | cut -d ' ' -f4)
	PERCENTAGE=$(echo "(100*$GOOD)/($GOOD+$BAD)"|bc)
	YLABEL="pew pew, $PERCENTAGE%!"
	WIDTH=1920
	HEIGHT=1080
	/srv/jenkins/bin/make_graph.py $CSV $PNG 3 "$XLABEL" "$YLABEL" $WIDTH $HEIGHT
fi

#
# cleanup
#
rm $DUMMY_FILE
