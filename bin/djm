#!/bin/bash
# vim: set noexpandtab:

#
# djm - documented jenkins maintenance
#
# Copyright 2023-2025 Holger Levsen <holger@layer-acht.org>
# released under the GPLv2
#

set -e
set -o pipefail		# see eg http://petereisentraut.blogspot.com/2010/11/pipefail.html

#
# define environment and global variables
#
DRY_RUN=false
VERBOSE=false
FETCH=false
NO_FETCH=false
NO_FUTURE=false
LOGFILE=~/.djm.log
FAIL_REASON=""
CONFIRM=false
BG=""
TARGET=
ACTION=
REASON=
COMMAND=
SHORTNODE=
NODE=
NTP_SERVER=
LOCAL_LOGFILE=~/.djm.log
UI_LOGFILE=~/.djm-jenkins-ui.log
PARSER_CACHE=~/.djm-jenkins-parser.cache
YEAR="$(date -u '+%Y')"
MONTH="$(date -u '+%m')"
LOGMONTH=${YEAR}-${MONTH}
JOBS=~/.djm-jobs.txt
if [ -z "$DJM_USER" ] ; then
	if [ -n "$DEBFULLNAME" ] ; then
		DJM_USER="$(echo $DEBFULLNAME | cut -d ' ' -f1 | tr '[:upper:]' '[:lower:]')"
	else
		echo "Environment variable DJM_USER (or DEBFULLNAME) must be set. Exiting."
		exit 1
	fi
fi

#
# little helpers
#
THREE_C="%42s  %12s  %12s\n"
TWO_C="%42s  %12s\n"
ONE_C="%42s  %12s\n"

#
# nodes running in the future
#
node_in_the_future () {
	case "$1" in
		ionos5-amd64*|ionos6-i386*|ionos15-amd64*|ionos16-i386*) true ;;
		codethink03*) true ;;
		osuosl2*) true ;;
		infom02*) true ;;
		*) false ;;
	esac
}

#
# parse parameters
#

# thanks to /usr/share/doc/util-linux/examples/getopt-parse.bash
TEMP=$(getopt -o 'dfhm:nrvy:' --long 'dry-run,fetch,help,month:,no-future,report,verbose,year:' -n 'djm' -- "$@")
if [ $? -ne 0 ]; then
	echo 'Terminating...' >&2
	exit 1
fi
# Note the quotes around "$TEMP": they are essential!
eval set -- "$TEMP"
unset TEMP
while true; do
	case "$1" in
		'-d'|'--dry-run')
			DRY_RUN=true
			shift
			continue
			;;
		'-f'|'--fetch')
			FETCH=true
			shift
			continue
			;;
		'-h'|'--help')
			MODE=help
			shift
			continue
			;;
		'-m'|'--month')
			if [[ ! "$2" =~ ^[0-9]+$ ]] || [ $2 -lt 1 ] || [ $2 -gt 12 ] ; then
				echo "Invalid --month specified: $2" >&2
				exit 1
			fi
			MONTH=$(printf "%02d" $2)
			NO_FETCH=true
			shift 2
			continue
			;;
		'-n'|'--no-future')
			NO_FUTURE=true
			shift
			continue
			;;
		'-r'|'--report')
			MODE=report
			shift
			continue
			;;
		'-v'|'--verbose')
			VERBOSE=true
			shift
			continue
			;;
		'-y'|'--year')
			if [[ ! "$2" =~ ^[0-9]+$ ]] || [ $2 -lt 2023 ] || [ $2 -gt 2342 ] ; then
				echo "Invalid --year specified: $2" >&2
				exit 1
			fi
			YEAR=$2
			NO_FETCH=true
			shift 2
			continue
			;;
		'--')
			shift
			break
			;;
		*)
			echo 'Internal error!' >&2
			exit 1
			;;
	esac
done
# some options don't make much sense
if $FETCH && $NO_FETCH ; then
	echo "Options --fetch and --month and/or --year together should not be combined."
	exit 1
fi
# show report if not called with any options
if [ -z "$MODE" ] ; then
	if [ -z "$1" ] ; then
		MODE=report
	else
		MODE=djm
	fi
fi

show_help(){
	separator "="
	djm_header
	separator "="
	echo
	echo "djm				shows report (and sometimes fetches remote data)"
	echo "djm -d/--dry-run *		will not actually do *"
	echo "djm -f/--fetch (*)		force fetching remote data (and optionally do *)"
	echo "djm -h/--help			shows this help"
	echo "djm -m/--month MONTH		define MONTH for showing report"
	echo "djm -n/--no-future *		do *, and then keep 'future hosts' running in the present"
	echo "djm -r/--report			shows report (and sometimes fetches remote data)"
	echo "djm -v/--verbose *		do * verbosely"
	echo "djm -y/--year YEAR		define YEAR for showing report"
	echo "djm TARGET ACTION REASON	do ACTION on all TARGET(s) for REASON, see below:"
	echo
	echo "TARGET= 'all' or grepable (jenkins, amd64, ionos, osuosl3) from ./nodes/list_nodes"
	echo "        or special target '.' for jenkins-ui ACTION."
	echo
	echo "ACTION= one of these:"
	echo "         a|autoremove            apt autoremove && apt clean"
	echo "         b|bring-back            mark hosts as online"
	echo "         c|command               run \$4 on hosts"
	echo "         cp|check-packages       run dsa-check-packages and enter bash on hosts"
	echo "         d|deploy-git            deploy jenkins.debian.net.git"
	echo "         f|fix-future            fix clock on hosts running in the future"
	echo "         j|jenkins-ui            for when doing things manually in the jenkins-ui"
	echo "         o|offline               mark hosts as offline"
	echo "         p|powercycle"
	echo "         r|reboot"
	echo "         rm|rmstamp              remove stamp files (from pbuilder and update|jdn.sh)"
	echo "         s|shell                 for manual maintenance"
	echo "         u|upgrade               apt update && apt -y dist-upgrade"
	echo
	echo "REASON= one of these:"
	echo "          cs=clock skew"
	echo "          dj=debug job problems"
	echo "          dh=debug host problems"
	echo "          hb=host is back"
	echo "          ho=host is overloaded"
	echo "          ku=kernel upgrade"
	echo "          np=no ping"
	echo "          nt=new things"
	echo "          rm=regular maintenance"
	echo "          su=security updates"
	echo
	if [ -n "$1" ] ; then
		separator
		echo
		echo Problem parsing parameters:
		echo "TARGET = $1"
		echo "ACTION = $2"
		echo "REASON = $3"
		# FIXME: $4 is not explained here
		echo
		echo Problems encountered:
		echo
		echo -e "$4"
		echo
	fi
}

verify_target_action_reason(){
	#
	# verify target, action & reason
	#
	TARGET=$1
	ACTION=$2
	REASON=$3
	case $TARGET in
		.)	TARGET="."
			;;
		all)	TARGET="$(`dirname $0`/../nodes/list_nodes )"
			;;
		*)	# it would be cool to support i15 = ionos15, same for osuosl and codethink...
			# echo c16|sed 's#c\([0-9][0-9]*\)#codethink\1#g'
			TARGET="$(`dirname $0`/../nodes/list_nodes | grep -E $TARGET || true)"
			;;
	esac
	if [ -z "$TARGET" ] ; then
		FAIL_REASON="${FAIL_REASON}Unknown target.\n"
	fi
	case $ACTION in
		a|autoremove)	ACTION=autoremove				;;
		b|bring-back)	ACTION=bring-back				;;
		c|command)	ACTION=command		;	CONFIRM=true	;;
		cp|check-packages)	ACTION=check-packages			;;
		d|deploy-git)	ACTION=deploy-git				;;
		f|fix-future)	ACTION=fix-future				;;
		j|jenkins-ui)	ACTION=jenkins-ui				;;
		o|offline)	ACTION=offline					;;
		p|powercycle)	ACTION=powercycle	;	CONFIRM=true	;;
		r|reboot)	ACTION=reboot		;	CONFIRM=true	;;
		rm|rmstamp)	ACTION=rmstamp					;;
		s|shell)	ACTION=shell					;;
		u|upgrade)	ACTION=apt-upgrade				;;
		*)		FAIL_REASON="${FAIL_REASON}Unknown action.\n" ;;
	esac
	case $REASON in
		cs)	REASON="clock skew"				;;
		dj)	REASON="debug job problems"			;;
		dh)	REASON="debug host problems"			;;
		hb)	REASON="host is back"				;;
		ho)	REASON="host is overloaded"			;;
		ku)	REASON="kernel upgrade"				;;
		np)	REASON="no ping"				;;
		nt)	REASON="new things"				;;
		rm)	REASON="regular maintenance"			;;
		su)	REASON="security updates"			;;
		*)	FAIL_REASON="${FAIL_REASON}Unknown reason.\n" ;;
	esac
	#
	# some targets only allow specific actions
	#
	if [ "$TARGET" = "." ] && [ "$ACTION" != "jenkins-ui" ] ; then
		FAIL_REASON="${FAIL_REASON}Target '.' can only be used with action 'jenkins-ui'.\n"
	elif [ "$TARGET" != "." ] && [ "$ACTION" = "jenkins-ui" ] ; then
		FAIL_REASON="${FAIL_REASON}Action 'jenkins-ui' can only be used with target '.'.\n"
	fi
}

djm_header() {
	echo "djm = documented jenkins maintenance"
	echo "      jenkins.debian.net exists since 2012, djm since 2023-04,"
	echo "      so expect changes for some time."
}

separator() {
	local CHAR="${1:--}"
	for i in $(seq 1 79) ; do
		echo -n "$CHAR"
	done
	echo
}

printf_if_not_zero() {
	if [ $2 -ne 0 ] ; then
		printf "$1" "$2" "$3"
	fi
}

printf_heading() {
	printf "$ONE_C" "$1"
	printf "$ONE_C" "$(for i in $(seq 1 ${#1}) ; do echo -n '-' ; done)"
}

djm_fetch() {
	#
	# parse jenkins build logs and copy the result over
	#
	local LOCK=${LOCAL_LOGFILE}.lock
	if [ ! -f ${LOCK} ] ; then
		touch $LOCK
		# run the parsers
		ssh $DJM_USER@jenkins.debian.net "DJM_USER=$DJM_USER /srv/jenkins/bin/djm-jenkins-parser"
		# fetch
		scp $DJM_USER@jenkins.debian.net:$(basename $UI_LOGFILE) $UI_LOGFILE
		scp $DJM_USER@jenkins.debian.net:$(basename $PARSER_CACHE) $PARSER_CACHE
		ssh $DJM_USER@jenkins.debian.net "cd ~jenkins/jobs ; ls -1d reproducible_* |wc -l" > $JOBS
		# split old data out
		if grep -q -v ^$LOGMONTH $LOGFILE || grep -q -v ^$LOGMONTH $UI_LOGFILE ; then
			figlet Splitting out old data...
			local file
			local month
			local tmpfile=$(mktemp)
			for file in $LOGFILE $UI_LOGFILE ; do
				for month in $(grep -v ^$LOGMONTH $file | cut -d '-' -f1-2 | sort -u) ; do
					echo "Moving old entries from $month in $file to $file.$month."
					grep ^$month $file >> $file.$month
					mv $file.$month $tmpfile
					sort -u $tmpfile > $file.$month
					grep -v ^$month $file > $tmpfile
					sort -u $tmpfile > $file
					rm $tmpfile
				done
			done
			# copy back cleaned logfile to jenkins server
			scp $UI_LOGFILE $DJM_USER@jenkins.debian.net:$(basename $UI_LOGFILE)
			rm -f $tmpfile
		fi
		rm $LOCK
	else
		echo "Warning: $LOCK exists, not fetching remote log files."
	fi
}

djm_report() {
	if [ "$LOGMONTH" != "${YEAR}-${MONTH}" ] ; then
		# use local variables to not cause a mess with djm_fetch()...
		local LOGMONTH=${YEAR}-${MONTH}
		local LOCAL_LOGFILE=~/.djm.log.${YEAR}-${MONTH}
		local UI_LOGFILE=~/.djm-jenkins-ui.log.${YEAR}-${MONTH}
	fi
	TMP_LOGFILE=$(mktemp)
	cat $LOCAL_LOGFILE $UI_LOGFILE | grep ^$LOGMONTH | sort -u > $TMP_LOGFILE
	TOTAL_ACTIONS=$(grep -c ^$LOGMONTH $TMP_LOGFILE)
	HOSTS=$(cut -d ',' -f2 $TMP_LOGFILE | sort -u | grep -v "web UI"| wc -l)
	TMP_WORKLOG=$(mktemp)
	( for point_in_time in $(cat $TMP_LOGFILE | cut -d ' ' -f1-2 | sort -u | sed -e 's#-##g' -e 's# ##g' -e 's#:##g' ) ; do
		x=$(( $point_in_time - 10 ))
		y=$(( $point_in_time + 15 ))
		for each_minute in $(seq $x $y) ; do
			echo $each_minute
		done
	done ) | sort -u >> $TMP_WORKLOG
	TOTAL_HOURS=0
	for point_in_time in $(cat $TMP_WORKLOG | cut -b 1-8 | sort -u) ; do
		echo -n "$point_in_time: "
		minutes=$(grep -c ^$point_in_time $TMP_WORKLOG)
		half_hours=$(( (30+$minutes)/30 ))
		hours=$(echo "scale=1 ; $half_hours/2" | bc)
		TOTAL_HOURS=$(echo "scale=1 ; $TOTAL_HOURS+$hours" | bc)
		echo "${hours}h  - ${TOTAL_HOURS}h"
	done
	rm $TMP_WORKLOG
	HOURS=$(cut -b 1-13 $TMP_LOGFILE | sort -u | wc -l)
	# create sorted list of actions/reasons (sorted by amount of occurrences)
	#  the character '%" is solely used as a delimiter for cut later
	#  the character '=' is added so later we can use a for-loop without dealing with $IFS
	ACTIONS=$(cut -d ',' -f3 $TMP_LOGFILE | sort | sed 's#^#%#g'| uniq -c | sort -nr | cut -d '%' -f2| sed -s "s# #=#g")
	REASONS=$(cut -d ',' -f4 $TMP_LOGFILE | sort | sed 's#^#%#g'| uniq -c | sort -nr | cut -d '%' -f2| sed -s "s# #=#g")

	separator "="
	echo "Still very simple statistics for djm logs:"
	echo " - $LOCAL_LOGFILE"
	echo " - $UI_LOGFILE"
	echo
	djm_header
	separator "="
	printf "$TWO_C" "month:" "$LOGMONTH"
	separator
	printf "$TWO_C" "djm actions:" "$TOTAL_ACTIONS"
	printf "$TWO_C" "hosts maintained:" "$HOSTS"
	printf "$TWO_C" "jenkins jobs maintained:" "$(cat $JOBS)"
	separator
	printf "$TWO_C" "hours with djm usage:" "${HOURS}"
	printf "$TWO_C" "hours worked:" "${TOTAL_HOURS}"
	separator "="

	printf "$ONE_C" "actions:"
	printf "$ONE_C" "--------"
	for action in $ACTIONS ; do
		grep_action="$(echo $action | sed -s 's#=# #g' | xargs echo)"
		AMOUNT=$(cut -d ',' -f3 $TMP_LOGFILE |grep "$grep_action"|wc -l)
		PERCENT=$((200*$AMOUNT/$TOTAL_ACTIONS % 2 + 100*$AMOUNT/$TOTAL_ACTIONS))
		printf "$THREE_C" "$grep_action:" "$PERCENT%" "($AMOUNT / $TOTAL_ACTIONS)"
	done
	separator "="

	printf "$ONE_C" "reasons:"
	printf "$ONE_C" "--------"
	for reason in $REASONS ; do
		grep_reason="$(echo $reason | sed -s 's#=# #g' | xargs echo)"
		AMOUNT=$(cut -d ',' -f4 $TMP_LOGFILE |grep "$grep_reason"|wc -l)
		PERCENT=$((200*$AMOUNT/$TOTAL_ACTIONS % 2 + 100*$AMOUNT/$TOTAL_ACTIONS))
		printf "$THREE_C" "$grep_reason:" "$PERCENT%" "($AMOUNT / $TOTAL_ACTIONS)"
	done
	separator "="

	#
	# statistics about jenkins jobs
	#
	PATTERNS="setup_schroot setup_pbuilder debian_live maintenance node_health_check
		html alpine openwrt .*fedora builds opensuse .*strap
		.*diffoscope reprotest disorderfs .*lfs" # .*(json|db|lfs)"
	PIPE_PATTERNS=$(echo $PATTERNS | sed 's# #|#g')
	HEADING="$(cat $TMP_LOGFILE | grep jenkins-ui | wc -l) jobs triggered manually:"
	printf_heading "$HEADING"
	(
		for i in $PATTERNS  ; do
			printf_if_not_zero "$TWO_C" $(grep -c reproducible_$i $TMP_LOGFILE) "reproducible_${i}_.*"
		done
		for i in $(cut -d ',' -f5 $TMP_LOGFILE | grep -v -E $PIPE_PATTERNS|sort -u) ; do
			printf_if_not_zero "$TWO_C" $(grep -c $i $TMP_LOGFILE) "$i"
		done
	) | sort -n -r

	separator "="
	rm $TMP_LOGFILE
}

get_arch_color() {
	#
	# define terminal background color based on architecture
	#
	case "$1" in
		*amd64*)		BG=lightgreen ;;
		*i386*)			BG=lightblue ;;
		*arm64*)		BG=orange ;;
		*armhf*)		BG=lightyellow ;;
		*riscv64*)		BG=purple ;;
		*jenkins.debian.*)	BG=yellow ;;
		rb-mail*)		BG=pink ;;
		*)			BG=white ;;
	esac
}

run_xterm2wait4node_comeback() {
	#
	# prepare tmpfile to be run in xterm
	#
	local FILE=$(mktemp --tmpdir=/tmp djm-XXXXXXXX)
	local SLEEP=77
	cat > $FILE << EOF
#!/bin/bash
echo -n "Initial ${SLEEP}s sleep..."
for i in $(seq 1 $SLEEP | xargs echo) ; do
	echo -n .
	sleep 1
done
echo " now trying ssh to $NODE"
while ! ssh $NODE 'uptime ; echo press enter, $SHORTNODE is back after $ACTION ; read a || true' ; do
	for i in $(seq 1 30 | xargs echo) ; do
		echo -n "."
		sleep 1
	done
	echo "...and trying again."
done
rm $FILE
EOF
	#
	# actually run ssh (via tmpfile) in xterm
	#
	echo xterm -class deploy-jenkins -bg $BG -fa 'DejaVuSansMono' -fs 10 -e "bash $FILE"
	xterm -T "$SHORTNODE / $ACTION ($FILE)" -class deploy-jenkins -bg $BG -fa 'DejaVuSansMono' -fs 10 -e "bash $FILE" &
}

run_command_futureproof_on_node_in_xterm() {
	COMMAND="( $COMMAND ) || ( echo press enter ; read a )"
	if node_in_the_future "$SHORTNODE" ; then
		COMMAND="sudo ntpdate -b $NTP_SERVER ; $COMMAND"
		if ! $NO_FUTURE ; then
			COMMAND="$COMMAND ; sudo date --set='+398 days +6 hours + 23 minutes'"
		fi
	fi
	xterm -T "$SHORTNODE / $ACTION" -class deploy-jenkins -bg $BG -fa 'DejaVuSansMono' -fs 10 -e "ssh -t $NODE \"$COMMAND\"" &
}

djm_do() {
	#
	# make sure we have target, action & reason
	#
	TARGET=$1
	ACTION=$2
	REASON=$3
	verify_target_action_reason $TARGET $ACTION $REASON
	# display help text in case of problems (or no params given)
	if [ -n "$FAIL_REASON" ] ; then
		show_help $1 $2 $3 "$FAIL_REASON"
		exit 1
	fi

	#
	# main
	#
	if $DRY_RUN ; then
		echo "###"
		echo "###"
		echo "###  running in dry-run mode, not doing anything for real."
		echo "###"
		echo "###"
	elif $CONFIRM ; then
		echo
		echo Please confirm you want to do this:
		echo
	fi
	
	if $DRY_RUN || $CONFIRM ; then
		for NODE in $TARGET ; do
			if [ "$NODE" != "." ] ; then
				SHORTNODE=$(echo $NODE | cut -d '.' -f1)
			else
				SHORTNODE="jenkins web UI"
			fi
			echo "$(date -u '+%Y-%m-%d %H:%M UTC'), $SHORTNODE, $ACTION, $REASON"
			if [ -n "$4" ] && [ "$ACTION" = "command" ] ; then
				echo " - command to be run is \"$4\""
			fi
		done
	fi
	
	if $DRY_RUN ; then
		echo
		echo "# dry-run end."
		exit 0
	elif $CONFIRM ; then
		echo
		echo -n "confirm: y/N: "
		read CONFIRM
		echo
		case $CONFIRM in
			y|Y)	: ;;
			*)	echo "Ok, aborting" ; exit 0 ;;
		esac
	fi
	
	LOGDATE="$(date -u '+%Y-%m-%d %H:%M UTC')"
	for NODE in $TARGET ; do
		if [ "$NODE" != "." ] ; then
			SHORTNODE=$(echo $NODE | cut -d '.' -f1)
		else
			SHORTNODE="jenkins web UI"
		fi
		if [ "$SHORTNODE" = "jenkins" ]; then
			NODE=root@jenkins.debian.net
		fi
		# node specific configuration
		get_arch_color $NODE
		case $SHORTNODE in
			osuosl*)	NTP_SERVER=time.osuosl.org ;;
			*)		NTP_SERVER=de.pool.ntp.org ;;
		esac
		#
		# action
		#
		case $ACTION in
			reboot)		case $SHORTNODE in
						XXXXosuosl4*)	echo "Skipping rebooting $NODE because josch is running something there...."
								# this is disabled, o4 can be rebooted again
								;;
						*)		( ssh $NODE "sudo reboot" || xterm -T "$SHORTNODE / $ACTION failed" -class deploy-jenkins -bg $BG -fa 'DejaVuSansMono' -fs 10 -e "echo -e 'ssh to $NODE failed, thus rebooting failed.\n\npress enter to continue' ; read a " )
								run_xterm2wait4node_comeback
					esac
				;;
			powercycle)	case $SHORTNODE in
						jenkins)	`dirname $0`/powercycle_x86_nodes.py jenkins
								;;
						ionos*)		node=$(echo $NODE | cut -b 6- | cut -d '-' -f1)
								`dirname $0`/powercycle_x86_nodes.py $node
								;;
						virt*-armhf-rb) echo "Sorry, I do not yet know how to handle $NODE, skipping."
								echo "But the steps are documented in README.infrastructure."
								echo "One can powercycle mst0X however...."
								exit 1
								;;
						*-armhf-rb)	`dirname $0`/powercycle_armhf_nodes.sh $NODE
								;;
						codethink*)	`dirname $0`/powercycle_arm64_nodes.sh $(echo ${SHORTNODE:9} | cut -d '-' -f1)
								;;
						osuosl*)	echo "We cannot powercycle nodes at OSUOSL on our own. Please contact #osuosl on libera.chat or send email to support@osuosl.org"
								exit 1
								;;
						infom*)		openstack server reboot --hard --verbose $(echo $SHORTNODE | cut -d '-' -f1)
								;;
						*)		echo "This should not happen, please investigate and fixup."
								exit 1
								;;
					esac
					run_xterm2wait4node_comeback
					;;
			shell)	xterm -T "$SHORTNODE / $ACTION" -class deploy-jenkins -bg $BG -fa 'DejaVuSansMono' -fs 10 -e "ssh $NODE" &
				;;
			jenkins-ui)	echo "Documenting one manual action in the jenkins-ui."
					;;
			offline)	(
						cd `dirname $0`/..
						sed -i "s#^\# investigation needed#\# investigation needed\n$NODE#" jenkins-home/offline_nodes
						git add jenkins-home/offline_nodes
						git commit -s -m "reproducible nodes: mark $SHORTNODE as offline"
					)
					;;
			bring-back)	TMPFILE=$(mktemp -u)
					OFF_NODES=~jenkins/offline_nodes
					echo "Trying to mark $SHORTNODE as online. This is the diff for $OFF_NODES on jenkins.debian.net:"
					ssh jenkins.debian.net "grep -v $SHORTNODE $OFF_NODES > $TMPFILE ; diff $OFF_NODES $TMPFILE ; mv $TMPFILE $OFF_NODES ; chown jenkins:jenkins $OFF_NODES"
					;;
			fix-future)	if $NO_FUTURE ; then
						echo "djm was called with --no-future and to fix hosts running in the future - this makes no sense, exiting."
						exit 1
					fi
					if node_in_the_future "$SHORTNODE" ; then
						COMMAND="date ; sudo ntpdate -b $NTP_SERVER"
						COMMAND="$COMMAND && sudo date --set='+398 days +6 hours + 23 minutes'"
						COMMAND="$COMMAND ; date -u ; echo press enter ; read a"
						xterm -T "$SHORTNODE / $ACTION" -class deploy-jenkins -bg $BG -fa 'DejaVuSansMono' -fs 10 -e "ssh $NODE \"$COMMAND\"" &
					else
						echo " - skipping $SHORTNODE, not running in the future."
						continue
					fi

					;;
			autoremove)	COMMAND="sudo apt-get autoremove"
					run_command_futureproof_on_node_in_xterm
					;;
			apt-upgrade)	COMMAND="sudo apt-get update && sudo apt-get -y dist-upgrade && sudo apt-get clean"
					run_command_futureproof_on_node_in_xterm
					;;
			deploy-git)	GIT_REPO="https://salsa.debian.org/qa/jenkins.debian.net.git"
					unset GITOPTS
					if node_in_the_future "$SHORTNODE"; then
						GITOPTS="-c http.sslVerify=false"
					fi
					# real command for running manually: cd ~jenkins-adm/jenkins.debian.net/ ; sudo -H -u jenkins-adm git pull ; ./update_jdn.sh
					read -r -d '' COMMAND <<-EOF || true
						set -e
						set -o pipefail
						export LANG=C
						cd ~jenkins-adm
						if [ ! -d jenkins.debian.net ]; then
							echo "Please run init_node manually once."
							exit 1
						else
							cd jenkins.debian.net
							sudo -H -u jenkins-adm git config pull.ff only
							sudo -H -u jenkins-adm git ${GITOPTS:-} pull $GIT_REPO
						fi
						./update_jdn.sh 2>&1 | sudo tee -a /var/log/jenkins/update_jdn.log
					EOF
					COMMAND="( $COMMAND ) || ( echo press enter ; read a )"
					xterm -T "$SHORTNODE / $ACTION" -class deploy-jenkins -bg $BG -fa 'DejaVuSansMono' -fs 10 -e "ssh -t $NODE \"$COMMAND\"" &
					;;
			rmstamp)	COMMAND="sudo rm -f /var/log/jenkins/*stamp"
					xterm -T "$SHORTNODE / $ACTION" -class deploy-jenkins -bg $BG -fa 'DejaVuSansMono' -fs 10 -e "ssh -t $NODE \"$COMMAND\"" &
					;;
			command)	COMMAND="$4"
					COMMAND="$COMMAND ; ( echo press enter ; read a )"
					run_command_futureproof_on_node_in_xterm
					;;
			check-packages)	COMMAND="dsa-check-packages | tr -d , ; bash"
					run_command_futureproof_on_node_in_xterm
					;;
			*)	echo unsupported action, sorry, please improve.
				exit 1
				;;
		esac
	
		#
		# log
		#
		local LOCK=${LOGFILE}.lock
		local LOGGED=false
		while ! $LOGGED ; do
			if [ ! -f $LOCK ] ; then
				echo "$LOGDATE, $SHORTNODE, $ACTION, $REASON" | tee -a $LOGFILE
				LOGGED=true
			else
				echo "$LOCK exists, waiting 3 seconds until retry."
				sleep 3
			fi
		done
	done
	
	echo Thank you for doing documented jenkins maintenance.
}

show_fixmes() {
	local BASEDIR="$(dirname "$(readlink -e $0)")"
	local TMPFILE=$(mktemp)
	rgrep FI[X]ME $BASEDIR/* | grep -v $BASEDIR/TODO | grep -v echo > $TMPFILE || true
	if [ -s $TMPFILE ] ; then
		echo
		echo Existing FIXME statements:
		echo
		cat $TMPFILE | sed "s#$PWD#...#g"
		echo
	fi
	rm -f $TMPFILE
	separator
}

djm_finish() {
	DUMMY=$(mktemp)
	touch -d "$(date -u -d "12 hours ago" '+%Y-%m-%d %H:%M') UTC" $DUMMY
	if ! $NO_FETCH && ( $FETCH || [ ! -f $JOBS ] || [ $DUMMY -nt $JOBS ] ) ; then
		if ! $DRY_RUN ; then
			echo "Fetching remote logs."
			djm_fetch
		fi
	elif ! $NO_FETCH ; then
		echo "Not fetching remote logs."
	fi
	rm $DUMMY
	if $VERBOSE ; then
		show_fixmes
	fi
}

#
# main
#
case $MODE in
	report)		djm_report
			djm_finish
			;;
	djm)		djm_do "$1" "$2" "$3" "$4"
			djm_finish
			;;
	help)		show_help
			;;
esac
